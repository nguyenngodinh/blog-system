class CommentsController < ApplicationController
    before_action :logged_in_user, only: [:create, :destroy]

    def create
       # @micropost = Micropost.find_by(params[:micropost_id])
       # @comment = @micropost.comments.new comment_params
       # @comment.user_id = current_user.id
       @comment = current_user.comments.new comment_params
       @comment.micropost_id = params[:micropost_id]
       
       if @comment.save
            flash[:success] = "Comment is created"
        else
            flash[:danger] = "Can not create comment"
        end
        redirect_to micropost_path params[:micropost_id]
    end

    protected
    def comment_params
        params.require(:comment).permit(:content, :micropost_id)
    end
    
end
