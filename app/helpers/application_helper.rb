module ApplicationHelper
	#Returns a full title a per-page basic
	def full_title(page_title = '')
		base_title = "Nguyen Ngo Dinh | Ruby on Rails"
		if page_title.empty?
			base_title
		else
			"#{page_title} | #{base_title}" 
		end
	end
end
