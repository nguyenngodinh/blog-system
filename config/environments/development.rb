SampleApp::Application.configure do
  config.cache_classes = false
  config.eager_load = false
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method = :smtp
  host = 'localhost:3000'         #local server
  config.action_mailer.default_url_options = {host: host}
  ActionMailer::Base.smtp_settings = {
    :address => 'smtp.gmail.com',
    :port      => 587,
    :authentication => :plain,
    :user_name      => 'luong.van.thang.nam@gmail.com',
    :password       => 'R9pZFXpd-vYGJdS',
    :domain         => 'gmail.com',
    :enable_starttls_auto => true
  }
  config.active_support.deprecation = :log
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true
end
