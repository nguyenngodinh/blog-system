SampleApp::Application.configure do
  config.cache_classes = true
  config.eager_load = true
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true
  config.serve_static_assets = false
  config.assets.js_compressor = :uglifier
  config.assets.compile = false
  config.assets.digest = true
  config.assets.version = '1.0'
  config.force_ssl = true
  config.log_level = :info
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method = :smtp
  host = 'https://blog-system-nguyenngodinh.herokuapp.com/'
  config.action_mailer.default_url_options = {host: host}
  ActionMailer::Base.smtp_settings = {
    :address => 'smtp.gmail.com',
    :port      => 587,
    :authentication => :plain,
    :user_name      => 'luong.van.thang.nam@gmail.com',
    :password       => 'R9pZFXpd-vYGJdS',
    :domain         => 'gmail.com',
    :enable_starttls_auto => true
  }
  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.log_formatter = ::Logger::Formatter.new
end
