require 'test_helper'

class CommentTest < ActiveSupport::TestCase
    def setup
        @user = users(:michael)
        @micropost = microposts(:orange)
        @comment = @micropost.comments.build(content: "Comment QWERTY", user_id: @user.id)
    end

    test "should be valid" do
        assert @comment.valid?
    end

    test "user id and micropost_id should be present" do
        @comment.user_id = nil
        assert_not @comment.valid?
        @comment.user_id = @user.id
        @comment.micropost_id = nil
        assert_not @comment.valid?
    end

    test "content should be present" do
        @comment.content = "   "
        assert_not @comment.valid?
    end

    test "content should be at most 140 characters" do
        @comment.content = "c" * 141
        assert_not @comment.valid?
    end

    test "order should be most recent first" do
        assert_equal Comment.first, comments(:most_recent)
    end

    

end
